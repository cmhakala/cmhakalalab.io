---
title: Rundown Cafe
description: Current Employment
company: Rundown Cafe
role: Bar Manager
start_date: "October 2021"
end_date: "Present"
order: "4"
---

_Bar Manager_
_Rundown Cafe; Kitty Hawk, NC — October 2021 - Present_

* Proficiently manage beverage inventory, optimizing orders based on demand, seasonal fluctuations, and inventory levels.
* Innovatively design menus that respond to seasonal preferences and customer expectations, enhancing the overall dining experience.
* Mentor and train new bartenders, bar backs, and servers, ensuring they meet performance standards and maintain excellent service.
* Execute opening and closing responsibilities meticulously, contributing to the seamless operation of the restaurant.
* Cultivate and maintain strong partnerships with local breweries, beer representatives, distributors, and distilleries, staying current with industry trends and offering a diverse and up-to-date product selection to our clientele.