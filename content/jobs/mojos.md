---
title: Mojo’s Philladeli
description: Mojo’s Philladeli
company: Mojo’s Philladeli
role: Bartender/Server
start_date: "July 2018"
end_date: "October 2020"
order: "1"
---

_Bartender/Server_ 
_Mojo’s Philladeli; Richmond, VA July 2018 - October 2020_

* Skillfully and energetically served both tables and bar patrons, consistently providing a positive and welcoming experience.
* Demonstrated extensive knowledge of cocktails, wines, and beers, enhancing the overall beverage service.
* Managed cash drawers totaling $300 for both the bar and the restaurant, ensuring accuracy and accountability.
* Successfully executed opening and closing duties, which encompassed handling payouts to other employees, maintaining pristine glassware, ensuring a clean and hygienic environment, changing kegs, and efficiently restocking inventory to predefined levels.
* Held the responsibility of a key holder, demonstrating trustworthiness and reliability in managing critical aspects of daily operations.