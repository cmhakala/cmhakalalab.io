---
title: Osaka Sushi and Steakhouse
description: Osaka
company: Osaka Sushi and Steakhouse
role: Bartender/Server
start_date: "June 2021"
end_date: "January 2022"
order: "3"
---

_Bartender/Server_ 
_Osaka Sushi and Steakhouse, Richmond, VA June 2021 - January 2022_

* Upheld the high standards of fine dining aesthetics and ambiance, ensuring a memorable experience for guests.
* Demonstrated expertise in crafting cocktails, extensive knowledge of spirits, wines, and beers, enhancing the beverage service.
* Innovatively developed and prepared craft cocktails, keeping the menu fresh and in line with seasonal trends.
* Expertly managed the opening and closing procedures of the restaurant, consistently meeting management's expectations and maintaining operational excellence.