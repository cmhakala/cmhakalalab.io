---
title: The Lilly Pad 
description: Lilly Pad
company: The Lilly Pad
role: Bartender
start_date: "September 2020"
end_date: "June 2021"
order: "2"
---

_Bartender_ 
_The Lilly Pad, Richmond, VA September 2020 - June 2021_

* Spearheaded creative development and design of signature and seasonal cocktails, contributing to an engaging and ever-evolving menu.
* Collaborated with servers during peak periods by expediting drink and food orders and efficiently managing table seating to maximize service efficiency.
* Assumed responsibility for the secure management of the cash drawer, oversaw servers' end-of-shift paperwork, and diligently completed closing tasks related to the drawer.
* Upheld the bar area to the highest standards, ensuring it was consistently clean, well-stocked, and fully operational.