
const config = {
  homePageName: "Claire Hakala Resume",
  homePageDescription: "Claire Hakala Resume Homepage",
  authorName: "Claire Hakala",
  linkedIn: "https://www.linkedin.com/in/TBD/",
  email: "cmhakala@gmail.com",
  authorDescription:
    "TBD",
  dev: {
      jobsdir: "./content/jobs",
      outdir: "./public"
  }
};

module.exports = config;
